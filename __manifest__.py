# -*- encoding: utf-8 -*-

{
    'name': 'Buon Appetito per Noi Theme',
    'description': 'Website theme for Buon Appetito per Noi by XSoftware',
    'category': 'Theme/Corporate',
    'sequence': 1000,
    'version': '1.0',
    'depends': ['website', 'website_theme_install'],
    'data': [
        'data/theme_default_data.xml',
	'views/templates.xml'
    ],
    'images': [
        'static/description/cover.png'
    ],
    'application': False,
}
